"""
Constant Variable file

This project is designed with younger students in mind.
Focusing on code functionality while scrolling past lines of variables is sometimes unproductive.
That is why I have seperated the global values so that the project as a whole is more readable 
and if the students themselves wish to change the size of the screen or other variables it is in a single place
and less likely to cause issues when helping / debugging.

"""

import pygame

# ===== Window Variables ======

WINDOW_WIDTH = 250
WINDOW_HEIGHT = 250
window_fps = 10 # dynamically measures gamespeed globally
WINDOW_BUFFER = 10

WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption("Custom Snake Game")


# ===== Play_Area Variables ======

PLAYER_AREA_BUFFER = 10
PLAY_AREA_WIDTH = WINDOW_WIDTH - 20
PLAY_AREA_HEIGHT = WINDOW_HEIGHT - 30
PLAY_AREA_X = PLAYER_AREA_BUFFER
PLAY_AREA_Y = PLAYER_AREA_BUFFER * 2

PLAY_AREA_RECT = pygame.Rect(PLAY_AREA_X, PLAY_AREA_Y,
 PLAY_AREA_WIDTH, PLAY_AREA_HEIGHT)

# ===== Snake Variables ======

SNAKE_WIDTH = 10
SNAKE_HEIGHT = 10

SNAKE_X = WINDOW_WIDTH //2 + 5 # Plus 10 to line everything up 
SNAKE_Y = WINDOW_HEIGHT // 2 + 5

SNAKE_HEALTH = 1
SNAKE_SPEED = 10
SNAKE_DIRECTION = "LEFT"
SNAKE_PREV_DIR = "RIGHT"
SNAKE_START_SIZE = 4

SNAKE_RECT = pygame.Rect(SNAKE_X, SNAKE_Y, SNAKE_WIDTH, SNAKE_HEIGHT)

# ===== Food Variables ======

FOOD_WIDTH = SNAKE_WIDTH
FOOD_HEIGHT = SNAKE_WIDTH
FOOD_X = PLAY_AREA_X + 10
FOOD_Y = PLAY_AREA_Y + 10
FOOD_HEALTH = 1

FOOD_RECT = pygame.Rect(FOOD_X, FOOD_Y, FOOD_WIDTH, FOOD_HEIGHT)

# ===== Font Variables ======

pygame.font.init() # Main file init does not initiate all of pygames assets

# List of fonts, uncomment a single line you wish to use
game_font = 'monkey'
#game_font = 'comicsans'
#game_font = 'sans-serif'

# Fonts to be used in the game
# Font position may need to be adjusted to accomdate new fonts
FONT_START = pygame.font.SysFont(game_font, 30)
FONT_INSTRUCT = pygame.font.SysFont(game_font, 15)
FONT_NAME = pygame.font.SysFont(game_font, 30)
FONT_POINTS = pygame.font.SysFont(game_font, 25)

# ===== Color Variables ======

COLOR_WHITE = (225, 255, 255)
COLOR_BLACK = (0, 0, 0)
COLOR_GRAY = (192, 192, 192)
COLOR_RED = (255, 0, 0)
COLOR_GREEN = (0, 255, 0)
COLOR_BLUE = (0, 0, 255)
COLOR_YELLOW = (255, 255, 0)
COLOR_PEACH = (238, 193, 255) 
COLOR_PURPLE = (134, 77, 246)

# ===== HIT Variables ======

EVENT_EAT_FOOD = pygame.USEREVENT + 1
EVENT_SNAKE_HIT = pygame.USEREVENT + 2

