"""
Snake File that contains the Snake (player) class
and makes up the bulk of the user experience of the game. 
"""

import pygame
import const as c
import food as f

class Snake:

    # Values not used by the current snake section but the next one in line 
    prev_x = 0
    prev_y = 0


    def __init__(self, color):

        # Initiates initial values for the snake object

        # Snake Head = Purple
        # Snake Body = Green
        self.color = color
       
       # Variables are set from constant files as a way of practicing using global variables
       # and to allow the students to adjust and change values away from functionality
        self.x = self.original_x = c.SNAKE_X
        self.y = self.original_y = c.SNAKE_Y
        self.width = c.SNAKE_WIDTH
        self.height = c.SNAKE_HEIGHT
        self.speed = c.SNAKE_SPEED
        self.dir = c.SNAKE_DIRECTION
        self.prev_dir = c.SNAKE_PREV_DIR
        self.health = c.SNAKE_HEALTH
        self.invul_frame = True

        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
        self.dead = False
       

    def draw(self):

        # Draws the snake on the screen
        pygame.draw.rect(c.WINDOW, self.color,
         (self.x, self.y, self.width, self.height))
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)

    # ===== End of Snake Class ======

# All method groups are in order of head -> body -> whole snake
# Methods within each group are in alphebetical order

# === Snake Head Methods ==== 
def snake_head_eat(section, food, snake_list):

    # Checks if the snakes head has collided with food
    if food.rect.colliderect(section.rect):
        pygame.event.post(pygame.event.Event(c.EVENT_EAT_FOOD))
        food.eaten()
        snake_whole_grow(snake_list)

    return section

def snake_head_direction(section, direction):

    # Updates snake head - In Main file -> Key presses
    # Ensures that the snake cannot go back on itself
    if section.dir == "LEFT" and direction != "RIGHT":
        section.dir = direction

    if section.dir == "RIGHT" and direction != "LEFT":
        section.dir = direction

    if section.dir == "UP" and direction != "DOWN":
        section.dir = direction

    if section.dir == "DOWN" and direction != "UP":
        section.dir = direction


def snake_head_move(section):

    # Sets current x + y to previous variables
    # This allows the rest of the body to follow behind
    section.prev_x = section.x
    section.prev_y = section.y
    
    # Moves snake along in straight line depending on the chosen direction
    # Only the direction can be changed by the player 
    # There is no way to stop the snake moving
    if section.dir == "LEFT":
        section.x -= c.SNAKE_SPEED
        
    if section.dir == "RIGHT":
        section.x += c.SNAKE_SPEED
        
    if section.dir == "UP":
        section.y -= c.SNAKE_SPEED
        
    if section.dir == "DOWN":
        section.y += c.SNAKE_SPEED
    

def snake_head_wall(section):
    
    # Checks if snake head has hit wall of play area
    if section.x < c.PLAY_AREA_X:
        # Snake bumps on LEFT wall
        section.health -= 1
    if section.x > c.PLAY_AREA_WIDTH:
        # Snake bumps on RIGHT wall
        section.health -= 1
    if section.y < c.PLAY_AREA_Y:
        # Snake bumps on TOP wall
        section.health -= 1
    if section.y > c.PLAY_AREA_Y + c.PLAY_AREA_HEIGHT - c.SNAKE_HEIGHT:
        # Snake bumps on BOTTOM wall
        section.health -= 1

# === Snake Body Methods ==== 
def snake_body_move(current_section, previous_section):

    # Allows the snake sections to follow the head or the previous section
    # Takes the current section that is being moved
    # and compares it to the previous sections x + y values

    # Assigns the current x and y value to the prev variables.
    # This allows the next section in the list to follow in this section's footsteps
    current_section.prev_x = current_section.x
    current_section.prev_y = current_section.y

    # Assigning the previous variables allows a comparison to be made
    # with the current x + y values, essentially meaning that the 
    # current x + y of the current section must match the prev x + y of 
    # the section in the lower index in the snake list.
    if current_section.x > previous_section.prev_x:
        # FOLLOWS LEFT
        current_section.x = previous_section.prev_x
        
    if current_section.x < previous_section.prev_x:
        # FOLLOWS RIGHT
        current_section.x = previous_section.prev_x
        
    if current_section.y < previous_section.prev_y:
        # FOLLOWS UP
        current_section.y = previous_section.prev_y

    if current_section.y > previous_section.prev_y:
        # FOLLOWS DOWN
        current_section.y = previous_section.prev_y


def snake_body_hit(section, head):

    # Checks each section of the snake (that is not the head or the one after)
    # to see if they have collided
    # Due to the way the movement works the head and the one direction after
    # are always colliding so it is excluded

    if section.rect.colliderect(head.rect):
        pygame.event.post(pygame.event.Event(c.EVENT_SNAKE_HIT))
        head.health -= 1


def snake_body_update(c_section, p_section):

    # Updates the body segments to follow the one before it
    # Only triggers when the previous index has a different direction
    if c_section.dir != p_section.dir:
        c_section.dir = p_section.dir


# === Snake Whole Methods ==== 
def snake_whole_grow(snake_list):

    # When a piece of food is eaten a new tail section is created
    # and appended using the previous as a template for pos and dir

    snake_tail = snake_list[-1]

    new_snake = Snake(c.COLOR_GREEN) # New snake section created

    # Uses current end of snake as a reference point
    new_snake.dir = snake_tail.dir
    new_snake.x = snake_tail.x
    new_snake.y = snake_tail.y

    snake_list.append(new_snake) # Adds new tail on end making it longer
        
    return snake_list


def snake_whole_loop(snake, food):

    i = 0 # While loop iterator

    # Loop checks
    # "For loop" was not used because I needed to use other indexes 
    # not just the current one in use
    while i < len(snake):

        # Different functions are seperated based on color (or role)
        # This allows the while loop to iterate faster compared to zero checks
        if snake[i].color == c.COLOR_PURPLE:
            # Performs checks on Head
            snake_head_eat(snake[i], food, snake)
            snake_head_move(snake[i])
            snake_head_wall(snake[i])
            
        if snake[i].color != c.COLOR_PURPLE:
            # Performs checks on Body and
            snake_body_update(snake[i], snake[i - 1])
            snake_body_move(snake[i], snake[i - 1])
            if snake[i -1].color != c.COLOR_PURPLE and snake[0].invul_frame == False:
                snake_body_hit(snake[i], snake[0])
                
        i += 1 # Check Indentation
    i = 0

    return snake


def snake_whole_reset(snake, body_size, food):

    # When the snake dies the game will reset to its initial state
    snake.clear()
    food.points = 0
    c.window_fps = 10
   
    return snake, food


def snake_whole_spawn(snake_list, body_size, start_health):

    # Sets up the snake head and body sections at the start of the game
    # The different "roles" of each section is allocated through color

    # Front of snake
    snake_head = Snake(c.COLOR_PURPLE)
    snake_list.append(snake_head)

    i = 0
    while i < body_size:

        # Body of snake is iterated and x value staggered 

        snake_body = Snake(c.COLOR_GREEN) # Snake section created

        # Values from previous section used as template
        snake_body.x = snake_list[-1].x + c.SNAKE_WIDTH
        snake_body.prev_x = snake_list[-1].x

        snake_list.append(snake_body) # Snake section added to body

        i += 1 # Exit condition of while loop

    return snake_list

