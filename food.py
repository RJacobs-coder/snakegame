"""
Food File containing the food class.
The goal of the game is to manipulate the snake to eat the food
while avoiding all the walls
"""

import pygame
import const as c
import random

class Food:

    def __init__(self):

        # Initiates initial values for the food object

        # Variables are set from constant files as a way of practicing using global variables
        # and to allow the students to adjust and change values themselves away from functionality
        self.x = c.FOOD_X
        self.y = c.FOOD_Y
        self.width = c.FOOD_WIDTH
        self.height = c.FOOD_HEIGHT
        self.color = c.COLOR_RED

        self.sector = 1
        self.points = 0

        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)

        
    def draw(self):

        # Draws the food on the screen
        pygame.draw.rect(c.WINDOW, self.color,
         (self.x, self.y, self.width, self.height))
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)


    def eaten(self):

        # Moves food to different location and adds point to total to represent consumption
        self.points += 1
        self.respawn()


    def respawn(self):

        # When the player eats the food it will respawn in a new location
        # The play area is split into 4 smaller squares (sectors)
        # A random number is used to determine which sector the food will spawn in
        random_sector = random.randrange(1,4)
        land_on = 10 # Rounds to nearest whole 10 number to line up with snake
        if random_sector != self.sector:

            if random_sector == 1: # TOP LEFT
                self.x = random.randrange(c.PLAY_AREA_X, c.PLAY_AREA_WIDTH // 2, land_on)
                self.y = random.randrange(c.PLAY_AREA_Y, c.PLAY_AREA_HEIGHT // 2, land_on)

            if random_sector == 2: # TOP RIGHT
                self.x = random.randrange(c.PLAY_AREA_WIDTH // 2, c.PLAY_AREA_WIDTH ,land_on)
                self.y = random.randrange(c.PLAY_AREA_Y,c.PLAY_AREA_HEIGHT // 2, land_on)

            if random_sector == 3: # BOTTOM LEFT
                self.x = random.randrange(c.PLAY_AREA_X, c.PLAY_AREA_WIDTH // 2, land_on)
                self.y = random.randrange(c.PLAY_AREA_HEIGHT // 2,c.PLAY_AREA_HEIGHT, land_on)

            if random_sector == 4: # BOTTOM RIGHT
                self.x = random.randrange(c.PLAY_AREA_WIDTH // 2, c.PLAY_AREA_WIDTH, land_on)
                self.y = random.randrange(c.PLAY_AREA_HEIGHT // 2,c.PLAY_AREA_HEIGHT, land_on)
        else:
            self.respawn() # Prevents food from spawning in same sector

    # ===== End of Food Class ======






