"""
Simple Snake Game Lesson Reference

Author:
Robert Jacobs
Freelance Programming / English Teacher
Hobby Developer

Audience:
Python Programming students ages 13+

Description:
Snake game where the user controls a snake around a playable space using WASD keys.
The SPACE BAR can be pressed to start the game and the No. 0 key can be used to exit.
The goal is to chase down the food while avoid the walls and the growing body of the snake itself.
Created to be used as a teaching project to learn about how games can be created.
Works on Mac, Windows and linux with the Pycharm IDE in mind.

Notes:
Readability / Teachability was considered a higher priority at the cost of some performance / optimization.
This project is used mainly as a reference and cannot be auto played in Pycharm without configuation.
Project can be run through the command line by parsing the main.py file.

Requires: 
Python3 3.11.5+
Pygame 2.5.2+

"""
import pygame # Allows use of game engine
import sys # Accesses host system to allow clean exit

# Custom Files
import const as c
import snake as s
import food as f

pygame.init() # Initiates starting modules for game engine (font not included)

def draw_window(snake, food):

    # Draws main gaming space and art assets

    # Window and Play Space Drawing
    c.WINDOW.fill(c.COLOR_BLACK) # First - Fills in background with a single color
    pygame.draw.rect(c.WINDOW, c.COLOR_GRAY, c.PLAY_AREA_RECT) # Gray player area

    # Ingame Assets Drawing
    for section in snake:
        # Draws each section of the snake
        section.draw()
    food.draw() # Check indentation

    # Text Drawing
    text_point = "POINTS: " + str(food.points)
    text_draw = c.FONT_POINTS.render(text_point, True, c.COLOR_BLUE) # Bool is for anti aliasing
    c.WINDOW.blit(text_draw, (0, 0))

    pygame.display.update() # Last - Updates screen with new variables

def reset_screen(text_one, text_two, text_color):

    # Displays a screen that instructs the player to 
    # press the space bar to begin / reset the game

    c.WINDOW.fill(c.COLOR_BLACK)# First - Fills in background for other things to be added

    # Compiles Variable values for text
    text_draw_one = c.FONT_START.render(text_one, True, text_color)
    text_draw_two = c.FONT_INSTRUCT.render(text_two, True, text_color)

    # Puts first text on screen - Can be one line, but harder for students to copy
    x_pos = c.WINDOW_WIDTH // 2 - text_draw_one.get_width() //2
    y_pos =  c.WINDOW_HEIGHT // 4
    c.WINDOW.blit(text_draw_one, (x_pos, y_pos)) # Drawing of text

    # Puts first text on screen - Can be one line, but harder for students to copy
    x_pos = c.WINDOW_WIDTH - (text_draw_two.get_width()) - 10
    y_pos =  c.WINDOW_HEIGHT // 4 + 40
    c.WINDOW.blit(text_draw_two, (x_pos, y_pos)) # Drawing of text
    
    pygame.display.update() # Last - Updates screen with all the values

def main():

    # Main method that contains main loop and global processes
    
    # Main Variables
    clock = pygame.time.Clock() # Sets game pace, will increase as snake gets longer
    run = True # Initiates main game loop
    
    snake_list = [] # Have to initiate variables first, even if empty
    direction = ""

    # Intial text at the start of the game
    text_one = "Welcome to Snake"
    text_two = "Press \"SPACE\" to start & \"0\" to Exit"
    text_color = c.COLOR_BLUE
    
    # Object Creation
    s.snake_whole_spawn(snake_list, c.SNAKE_START_SIZE, 1)
    food = f.Food()
    
    # Flags provide "pauses" in the main loop until funcation is ready again
    start_flag = False
    eaten_flag = False
    direction_flag = False

    while run:

        clock.tick(c.window_fps) # Sets iteration speed of while loop

        for event in pygame.event.get(): # Cycles through event array 

            if event.type == pygame.QUIT:

                # Allows clean exit of game 
                pygame.quit()
                sys.exit()
                run = False

            if event.type == pygame.KEYDOWN:

                # ===== Snake Movement=====
                if event.key == pygame.K_a:

                    # Points snake LEFT
                    s.snake_head_direction(snake_list[0], "LEFT")

                if event.key == pygame.K_d:

                    # Points snake RIGHT
                    s.snake_head_direction(snake_list[0], "RIGHT")

                if event.key == pygame.K_w:

                    # Points snake UP
                    s.snake_head_direction(snake_list[0], "UP")

                if event.key == pygame.K_s:

                    # Points snake DOWN
                    s.snake_head_direction(snake_list[0], "DOWN")

                # ===== Other Keyboard inputs===== 
                if event.key == pygame.K_SPACE:

                    start_flag = True # Triggers start of game loops
                    
                    # Updates screen text from start screen to reset screen
                    text_one = "OH NOES YOU DIED!!!!"
                    text_two = "Press \"SPACE\" to restart or \"0\" to Exit"
                    text_color = c.COLOR_RED

                if event.key == pygame.K_0:

                    # Exit Game Manually
                    pygame.quit()
                    sys.exit()
                    run = False

        
        # Main Method Calls (Check Indentation)

        # User Functionality
        keys_pressed = pygame.key.get_pressed()

        # Snake Functionality
        if start_flag == True:

            # Flag turns on main game loop.
            # When True the game has started and the user can control the snake
            # When False the game has ended and the reset screen is displayed

            if snake_list[0].health <= 0: # Checks if snake is alive
                # When the snake has died it deletes and creates new snake
                s.snake_whole_reset(snake_list, c.SNAKE_START_SIZE, food)
                s.snake_whole_spawn(snake_list, c.SNAKE_START_SIZE, 1)

                # Invul frame helps snake not take damage as it respawns
                snake_list[0].invul_frame = True
                start_flag = False # Once creation of snake is complete, moves onto other loop
                

            else:
                # Retains initial invul frame until a single bit of food is eaten
                # The invul frame prevents snake from colliding, does not effect walls
                if food.points > 0:
                    snake_list[0].invul_frame = False
                    
                # Single method that controls the whole game
                s.snake_whole_loop(snake_list, food)

            # Updates and draws window of game state at each frame
            draw_window(snake_list, food)
          
        else:
            # When the game is not in progress the reset screen is displayed
            reset_screen(text_one, text_two, text_color)
         

        # Other Functionality
        # Controls snake speed by manipulating the FPS
        # The FPS controls the speed of the main while, faster FPS means faster game
        if len(snake_list) > c.window_fps and c.window_fps < 15:
            c.window_fps += 1

if __name__ == "__main__":
    # This section is the first to be exectuted in the program
    # Everything above it loaded into memory first
    main()

